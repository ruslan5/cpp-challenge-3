#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    loadYear();
}


void MainWindow::loadYear()
{
    int year = ui->lineEdit->text().toInt();
    bool isIntercalaryYear = false;
    QString result;

    if (year >= 1 && year <= 3000) {

        if ((year % 4) == 0) {

            if ((year % 100) != 0 || (year % 400) == 0) {
                isIntercalaryYear = true;
            }
        }

        if (isIntercalaryYear) {
            result = "It is the intercalary year";
        } else {
            result = "It is not the intercalary year";
        }
    } else {
        result = "Invalid year. It must be between 1 and 3000";
    }

    ui->label_4->setText(result);
}

